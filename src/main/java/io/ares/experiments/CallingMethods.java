package io.ares.experiments;

/**
 * @author Dan Miles
 */
public class CallingMethods {

    public static void main(String args[]){
        String name = "Luke Attwood";
        char c = name.charAt(3);
        int length = name.length();
        System.out.println("Hello " + name);
    }

}

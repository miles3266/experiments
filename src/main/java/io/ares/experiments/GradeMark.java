package io.ares.experiments;

import java.util.Scanner;

public class GradeMark {

    public static void main(String args[]){
        while (true){
            Scanner sc = new Scanner(System.in);
            int i = sc.nextInt();
            checkResults(i);
        }
    }

    private static void checkResults(Integer mark){
        if (isPass(mark)){
            System.out.println("User mark passed!");
        } else {
            System.out.println("User mark did not pass");
        }
        System.out.println("Users grade is a " + getGrade(mark));
    }

    private static String getGrade(Integer mark){
        final Integer passBoundary = 40;
        final Integer pass = 60;
        final Integer merit = 70;
        if (mark <= passBoundary){
            return "Fail";
        } else if (mark < pass){
            return "Pass";
        } else if (mark <= merit && mark >= pass){
            return "Merit";
        } else if (mark > merit){
            return "Distinction";
        }
        return null;
    }

    private static boolean isPass(Integer mark){
        final Integer passmark = 40;
        return mark > passmark;
    }

}
